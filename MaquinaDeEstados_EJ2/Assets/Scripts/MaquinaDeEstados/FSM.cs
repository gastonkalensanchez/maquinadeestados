using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{

    EnemigoEstadoBase estado_actual;
    public void Comenzar()
    {
        this.estado_actual=IniciarEstado();
    }
    public void Actualizar()
    {
        if (this.estado_actual!=null) this.estado_actual.Actualizar(); 
    }
    public void CambiarEstado(EnemigoEstadoBase nuevo_estado)
    {
        this.estado_actual.Terminar();
        this.estado_actual=nuevo_estado;
        this.estado_actual.Inicio();
    }
    protected virtual EnemigoEstadoBase IniciarEstado()
    {  
        return null;
    }


}


