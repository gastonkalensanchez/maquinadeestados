using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoAtaque : EnemigoEstadoBase
{
    public EnemigoAtaque(MovimientoEnemigo m_e):base("Enemigoataque",m_e){}
    public override void Inicio()
    {
        base.Inicio();

    }
    public override void Actualizar()
    {
        base.Actualizar();
        if (!((MovimientoEnemigo)m_e).npcscerca())
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._ENestado_idle);
        }else
        {
            ((MovimientoEnemigo)m_e).transform.LookAt(((MovimientoEnemigo)m_e).npcactivo.transform);
            ((MovimientoEnemigo)m_e).transform.Translate(Vector3.forward * 1.5f * Time.deltaTime);
        }

    }
    public override void Terminar()
    {
        base.Terminar();
    }



}
