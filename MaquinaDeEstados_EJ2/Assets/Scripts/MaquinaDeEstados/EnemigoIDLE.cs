using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoIDLE : EnemigoEstadoBase
{
   public EnemigoIDLE(MovimientoEnemigo m_e):base("enemigoidle",m_e){}

    public override void Inicio()
    {
        base.Inicio();
        ((MovimientoEnemigo)m_e).gameObject.GetComponent<Renderer>().material = ((MovimientoEnemigo)m_e).MaterialInfectado;
        ((MovimientoEnemigo)m_e).gameObject.tag = "INFECTADO";
        if ((GameManager.instancia.NPCs.Contains(((MovimientoEnemigo)m_e).gameObject)))
        {
            GameManager.instancia.NPCs.Remove(((MovimientoEnemigo)m_e).gameObject);
        }

    }
    public override void Actualizar()
    {
        base.Actualizar();

        if (((MovimientoEnemigo)m_e).npcscerca())
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._ENestado_ataque);
            return;
        }
        if (!((MovimientoEnemigo)m_e).EstaInfectado)
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._estado_idle);
            return;
        }
        ((MovimientoEnemigo)m_e).MovimientoRandom(1);
        
        

    }
    public override void Terminar()
    {
        base.Terminar();
    }
     

    

}
